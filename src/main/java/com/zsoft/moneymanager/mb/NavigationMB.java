package com.zsoft.moneymanager.mb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;


@ManagedBean
@RequestScoped
public class NavigationMB {
    final Logger logger = LoggerFactory.getLogger(NavigationMB.class);

    public String redirectToSignIn(){
        return redirectTo("pages/public/auth/signin.xhtml");
    }
    public String redirectToProfile(){
        return redirectTo("pages/protected/profile.xhtml");
    }
    public String redirectToSignUp(){
        return redirectTo("pages/public/auth/signup.xhtml");
    }

    private String redirectTo(String destination){
        try{
            FacesContext.getCurrentInstance().getExternalContext().redirect(destination);
        }catch (IOException e ){
        }
        return destination;
    }
}

package com.zsoft.moneymanager.api;

import com.zsoft.moneymanager.model.Expense;
import com.zsoft.moneymanager.service.ExpenseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/expense")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class ExpenseWS {
    final Logger logger = LoggerFactory.getLogger(ExpenseWS.class);

    /**
     * We need to specify the JNDI name of the resource so the Conteiner is able to 
     * inject the EJB 
     */
    @EJB(mappedName = "java:module/ExpenseService")
    ExpenseService expenseService;

    /**
     * Return an array of JSON objects representing all expenses registered
     *
     * @return A list with all expenses
     */
    @GET
    @Path("/all")
    public Response getExpenses() {
        return Response.ok(expenseService.findAll()).build();
    }

    /**
     * Return an specific expense based on the ID
     *
     * @param id The expense ID
     * @return The expense
     */
    @GET
    @Path("/{id}")
    public Response getExpense(@PathParam("id") final String id){
        return Response.ok(expenseService.find(id)).build();
    }

    /**
     * Receives a JSON object representing a new expense and persist it to the database
     *
     * @param expense The expense to be persisted
     * @return The new expense
     */
    @POST
    public Response create(final Expense expense) {
        expenseService.create(expense);
        return Response.ok(expense).build();
    }

    /**
     * Update an existing expense
     *
     * @param expense The expense to be updated
     * @return The updated expense
     */
    @PUT
    public Response update(final Expense expense){
        expenseService.update(expense);
        return Response.ok(expense).build();
    }

    /**
     * Delete an existing expense
     *
     * @param id The id of the expense
     * @return a message indicating the result
     */
    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") final String id){
        if(expenseService.delete(id))
            return Response.ok("Expense with ID = " + id + " was removed!").build();
        else
            return Response.status(404).build();

    }

}

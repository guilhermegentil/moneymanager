package com.zsoft.moneymanager.setup;

import com.zsoft.moneymanager.api.ExpenseWS;

import javax.ejb.Stateless;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@Stateless
public class ApplicationConfiguration extends Application {

    /**
     * Register the application JAX-RS services
     * @return A set containing all the application JAX-RS services
     */
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(ExpenseWS.class);
        return s;
    }

}
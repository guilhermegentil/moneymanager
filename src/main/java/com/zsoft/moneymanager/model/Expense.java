package com.zsoft.moneymanager.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Calendar;

@Entity
@NamedQueries({
        @NamedQuery(name = Expense.Query.FIND_ALL, query = "SELECT e FROM Expense e"),
        @NamedQuery(name = Expense.Query.FIND, query = "SELECT e FROM Expense e WHERE e.id = :id"),
})
@Table(name = "expense")
public class Expense implements Serializable{

    public static class Query {
        public static final String FIND_ALL = "Expense.findAll";
        public static final String FIND = "Expense.find";
    }

    @Id
    private String id;

    @NotNull
    @Size(max = 255)
    private String description;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar registered;

    @NotNull
    private double value;

    // Getters & Setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getRegistered() {
        return registered;
    }

    public void setRegistered(Calendar registered) {
        this.registered = registered;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Expense{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", registered=" + registered +
                ", value=" + value +
                '}';
    }
}

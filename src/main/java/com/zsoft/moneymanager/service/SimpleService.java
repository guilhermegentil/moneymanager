package com.zsoft.moneymanager.service;

import javax.ejb.Local;
import javax.ejb.Stateless;

@Local
@Stateless
public class SimpleService {

    private String helloWorld = "Hello World!";

    public String getHelloWorld(){
        return helloWorld;
    }
}

package com.zsoft.moneymanager.service;

import com.zsoft.moneymanager.model.Expense;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.UUID;

@Local
@Stateless
public class ExpenseService {
    final Logger logger = LoggerFactory.getLogger(ExpenseService.class);

    @PersistenceContext
    private EntityManager em;

    /**
     * Find all expenses
     *
     * @return A list with all expenses
     */
    public List<Expense> findAll(){
        return em.createNamedQuery(Expense.Query.FIND_ALL).getResultList();
    }

    /**
     * Find one specific expense
     *
     * @return One specific expense
     */
    public Expense find(String id){
        Query q = em.createNamedQuery(Expense.Query.FIND);
        q.setParameter("id", id);

        return (Expense) q.getSingleResult();
    }

    /**
     * Create a new expense
     *
     * @param expense The expense to be persisted
     * @return The persisted expense
     */
    public Expense create(final Expense expense){
        expense.setId(UUID.randomUUID().toString().toLowerCase());
        em.persist(expense);

        return expense;
    }

    /**
     * Update an existing expense
     *
     * @param expense The expense to be updated
     * @return The updated expense
     */
    public Expense update(final Expense expense){
        if(expense.getId() != null || !expense.getId().equals(""))
            em.merge(expense);

        return expense;
    }

    /**
     * Delete an existing expense
     *
     * @param id The ID of the expense to be deleted
     * @return True if the deletion was successful and False otherwise
     */
    public boolean delete(final String id){
        try {
            Expense e = find(id);
            em.remove(e);
        }catch(NoResultException e){
            return false;
        }
        return true;
    }



}

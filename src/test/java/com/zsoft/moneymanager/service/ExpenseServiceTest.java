package com.zsoft.moneymanager.service;

import com.zsoft.moneymanager.model.Expense;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJB;
import java.io.File;
import java.util.Calendar;

@RunWith(Arquillian.class)
public class ExpenseServiceTest {

    @EJB
    private ExpenseService expenseService;

    @Deployment
    public static Archive<?> createTestArchive() {
        return ShrinkWrap.create(WebArchive.class, "ExpenseServiceTest.war")
                .addClasses(ExpenseService.class, Expense.class)
                .addAsResource(new File("src/test/resources/test-persistence.xml"), "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void should_create_an_expense() {
        Expense e = new Expense();
        e.setDescription("new expense");
        e.setRegistered(Calendar.getInstance());
        e.setValue(10.0);

        expenseService.create(e);
        Assert.assertTrue(expenseService.find(e.getId()) != null);
    }


    @Test
    public void should_create_and_update_an_expense() {
        Expense e = new Expense();
        e.setDescription("new expense");
        e.setRegistered(Calendar.getInstance());
        e.setValue(10.0);

        expenseService.create(e);

        if(expenseService.find(e.getId()) != null) {
            String newDescription = "new description";

            e.setDescription(newDescription);
            expenseService.update(e);

            Assert.assertEquals(e.getDescription(), newDescription);
        }else{
            Assert.assertTrue("Failed to create expense!", false);
        }

    }

}
